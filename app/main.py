from tools.console import clear

# Importation des exo2.11
from chapitres.variables.exo2_11_1 import*
from chapitres.variables.exo2_11_2 import*
from  chapitres.variables.exo2_11_3 import*

#Importation des exo3.6
from chapitres.affichage.exo3_6_1 import*
from chapitres.affichage.exo3_6_2 import*
from chapitres.affichage.exo3_6_3 import*
from chapitres.affichage.exo3_6_4 import*
from chapitres.affichage.exo3_6_5 import*

#Importation des exo4.10
from chapitres.liste_range.exo4_10_1 import*
from chapitres.liste_range.exo4_10_2 import*
from chapitres.liste_range.exo4_10_3 import*
from chapitres.liste_range.exo4_10_4 import*

#Importation des exo5.5
from chapitres.boucle.exo5_4_1 import*
from chapitres.boucle.exo5_4_2 import*
from chapitres.boucle.exo5_4_3 import*
from chapitres.boucle.exo5_4_4 import*
from chapitres.boucle.exo5_4_5 import*
from chapitres.boucle.exo5_4_6 import*
from chapitres.boucle.exo5_4_7 import*
from chapitres.boucle.exo5_4_8 import*
from chapitres.boucle.exo5_4_9 import*
from chapitres.boucle.exo5_4_10 import*
from chapitres.boucle.exo5_4_11 import*
from chapitres.boucle.exo5_4_12 import*
from chapitres.boucle.exo5_4_13 import*
from chapitres.boucle.exo5_4_14 import*

#Importation des exo6.7
from chapitres.tests.exo6_7_1 import*
from chapitres.tests.exo6_7_2 import*
from chapitres.tests.exo6_7_3 import*
from chapitres.tests.exo6_7_4 import*
from chapitres.tests.exo6_7_5 import*
from chapitres.tests.exo6_7_6 import*
from chapitres.tests.exo6_7_7 import*
from chapitres.tests.exo6_7_8 import*
from chapitres.tests.exo6_7_9 import*
from chapitres.tests.exo6_7_10 import*

"""_summary_
Endpoint de votre programme.
"""

menu=0
while menu!="6":
    print("----------------------------------------")
    print(" Bienvenu dans le menu ") 
    while True:
        print()
        
        print(""" Voici les chapitres disponibles:
        1-Variables
        2-Affichage
        3-Liste
        4-Boucle
        5-Tests
        6-Exit
        """)
        menu=input("Faites votre choix: ")
    
        print("----------------------------------------")
    
    #Controle de saisie 
   
        if not menu.isdigit():
            print ("Saisie erronné :(")
            continue
        else:
            break
        
#Choix du chapitre
    choix=0
    if menu=="1":
        #Affichage du dossier Variables avec ses exercices
        print("""Voici les exercices dans ce chapitre 
                 1-Nombre de Friedman
                 2-Prédiction de résulat:opérations
                 3-Prédiction de résultat : opérations et conversions de types
                 """)
       #Choix de l'exercice à executer
        choix=input("Faites votre choix parmis ces exercices: ")
        print()
       
        if choix=="1":
            print(f"Vous avez choisi l'exercice numero {choix}:Nombre de Friedman")
            print()
            Friedman()
        elif choix=="2":
            print(f"Vous avez choisi l'exercice numero {choix}: Prédiction de résulat:opérations")
            print()
            OperationResult()
        elif choix=="3":
            print(f"Vous avez choisi l'exercice numero {choix}: Prédiction de résultat : opérations et conversions de types")
            print()
            Conversion()
            
    elif menu=="2":
        #Affichage du dossier Affichage avec ses exercices
        print("""Voici les exercices dans ce chapitre 
                 1-Affichage dans un programme
                 2-Poly-A
                 3-Poly-A et Poly-GC
                 4-Ecriture formatée
                 5-Ecriture formatée 2
                 """) 
        choix=input("Faites votre choix parmis ces exercices: ")
        print()
        
        #Choix de l'exercice à executer
        
        if choix=="1":
            print(f"Vous avez choisi le numero {choix}: Affichage dans un programme")
            print()
            exo3_1()
        elif choix=="2":
            print(f"Vous avez choisi le numero {choix}: Poly-A")
            print()
            exo3_2()
        elif choix=="3":
            print(f"Vous avez choisi le numero {choix}:Poly-A et Poly-GC")
            print()
            exo3_3()
        elif choix=="4":
            print(f"Vous avez choisi le numero {choix}:Ecriture formatée")
            print()
            exo3_4()
        elif choix=="5":
            print(f"Vous avez choisi le numero {choix}:Ecriture formatée 2")
            print()
            exo3_5()
            
    elif menu=="3": 
        #Affichage du dossier Liste avec ses exercices
        print("""Voici les exercices dans ce chapitre 
                 1-Jour de la semaine
                 2-Saisons
                 3-Table de multiplication par 9
                 4-Nombres pairs
                 """)
        choix=input("Faites votre choix parmis ces exercices: ")
        print()
       
       #Choix de l'exercice à executer
        if choix=="1":
            print(f"Vous avez choisi l'exercice numero {choix} : Jour de la semaine")
            print()
            exo4_1()
        elif choix=="2":
            print(f"Vous avez choisi l'exercice numero {choix} : Saisons")
            print()
            exo4_2()
        elif choix=="3":
            print(f"Vous avez choisi l'exercice numero {choix} : Table de multiplication par 9")
            print()
            exo4_3()
        elif choix=="4":
            print(f"Vous avez choisi l'exercice numero {choix} : Nombres pairs")
            print()
            exo4_4()
            
    elif menu=="4":
        #Affichage du dossier boucle avec ses exercices
            print("""
                Voici les exercices dans ce chapitre 
                 1-Boucle de base
                 2-Boucle et jour de la semaine
                 3-Nombre 1 à 10 sur une ligne
                 4-Nombres pairs et impairs
                 5-Calcul de la moyenne
                 6-Produits de nombre consécutifs
                 7-Triangle
                 8-Triangle inversé
                 9-Triangle gauche
                 10-Pyramide
                 11-Parcours de matrice
                 12-Parcours de demi-matrice sans la diagonale
                 13-Sauts de puce
                 14-Suite de Fibonacci
                 """)
            
            choix=input("Faites votre choix parmis ces exercices: ")
            print()
            
            #Choix de l'exercice à executer
            if choix=="1":
                print(f"Vous avez choisi l'exercice numero {choix} : Boucle de base")
                print()
                exo5_1()
            elif choix=="2":
                print(f"Vous avez choisi l'exercice numero {choix} : Boucle et jour de la semaine")
                print()
                exo5_2()
            elif choix=="3":
                print(f"Vous avez choisi l'exercice numero {choix} : Nombre 1 à 10 sur une ligne")
                print()
                exo5_3()
            elif choix=="4":
                print(f"Vous avez choisi l'exercice numero {choix} : Nombres pairs et impairs")
                print()
                exo5_4()
            if choix=="5":
                print(f"Vous avez choisi l'exercice numero {choix} : Calcul de la moyenne")
                print()
                exo5_5()
            elif choix=="6":
                print(f"Vous avez choisi l'exercice numero {choix} : Produits de nombre consécutifs")
                print()
                exo5_6()
            elif choix=="7":
                print(f"Vous avez choisi l'exercice numero {choix} : Triangle")
                print()
                exo5_7()
            elif choix=="8":
                print(f"Vous avez choisi l'exercice numero {choix} : Triangle inversé")
                print()
                exo5_8()
            if choix=="9":
                print(f"Vous avez choisi l'exercice numero {choix} : Triangle gauche")
                print()
                exo5_9()
            elif choix=="10":
                print(f"Vous avez choisi l'exercice numero {choix}: Pyramide")
                print()
                exo5_10()
            elif choix=="11":
                print(f"Vous avez choisi l'exercice numero {choix}:Parcours de matrice ")
                print()
                exo5_11()
            elif choix=="12":
                print(f"Vous avez choisi l'exercice numero {choix}: Parcours de demi-matrice sans la diagonale")
                print()
                exo5_12()
            elif choix=="13":
                print(f"Vous avez choisi l'exercice numero {choix}: Sauts de puce")
                print()
                exo5_13()
            elif choix=="14":
                print(f"Vous avez choisi l'exercice numero {choix}: Suite de Fibonacci")
                print()
                exo5_14()
                
    elif menu=="5":
        #Affichage du dossier boucle avec ses exoercices
            print("""Voici les exercices dans ce chapitre 
                 1-Jour de la semaine
                 2--Séquence complémentaire d'un brin d'ADN
                 3-Minimun d'une liste
                 4-Fréquence des acides aminés
                 5-Notes et mention d'un étudiant
                 6-Nombres pairs
                 7-Conjecture de Syracuse
                 8-Attribution de la steucture secondaire des AA d'une protéine
                 9-Nomnbre premiers inférieur à 100
                 10-Recherche par dichotomie
                 
                 """)
            choix=input("Faites votre choix parmis ces exercices: ")
            print()
            
            
            #Choix de l'exercice à executer
            if choix=="1":
                print(f"Vous avez choisi l'exercice numero {choix} : Jour de la semaine")
                print()
                exo7_1()
            elif choix=="2":
                print(f"Vous avez choisi l'exercice numero {choix} : Séquence complémentaire d'un brin d'ADN")
                print()
                exo7_2()
            elif choix=="3":
                print(f"Vous avez choisi l'exercice numero {choix}: Minimun d'une liste")
                print()
                exo7_3()
            elif choix=="4":
                print(f"Vous avez choisi l'exercice numero {choix}: Fréquence des acides aminés")
                print()
                exo7_4()
            if choix=="5":
                print(f"Vous avez choisi l'exercice numero {choix}: Notes et mention d'un étudiant")
                print()
                exo7_5()
            if choix=="6":
                print(f"Vous avez choisi l'exercice numero {choix} : Nombres pairs")
                print()
                exo7_6()
            elif choix=="7":
                print(f"Vous avez choisi l'exercice numero {choix} : Conjecture de Syracuse")
                print()
                exo7_7()
            elif choix=="8":
                print(f"Vous avez choisi l'exercice numero {choix} : Attribution de la structure secondaire des Acides Aminés d'une protéine")
                print()
                exo7_8()
            elif choix=="9":
                print(f"Vous avez choisi l'exercice numero {choix} : Nomnbre premiers inférieur à 100")
                print()
                exo7_9()
            if choix=="10":
                print(f"Vous avez choisi l'exercice numero {choix} : Recherche par dichotomie")
                print()
                exo7_10()
   
   #Arrêt de la boucle infinie
    elif menu=="6":
        clear() 
print("Merci pour votre viste. A bientôt !")
      