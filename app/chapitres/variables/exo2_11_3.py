def Conversion():
    print(f" str(4) * int(`3`) => resultat:444 ")
    print(f" int(`3`) + float(`3.2`)=> resultat:6.2")
    print(f" str(3) * float(`3.2`) => resulat: il y a erreur , str et float ne peuvent pas interagir ensemeble")
    print(f" str(3/4) * 2) => resultat: 0.750.75 ")
    
    print()
    print("--------------------------------------")
    
    print("Resultat de l'interpreteur: ")
    print(f'str(4) * int("3") => {str(4) * int("3")}')
    print(f'int("3") + float("3.2") => {int("3") + float("3.2")}')
    print()
    print("""str(3/4) * 2)
      Traceback (most recent call last):
      File "/home/megumi/Documents/GEIT/L2/Python/Exo/variable(2)/exo2.11.3.py", line 3, in <module>
      print(str(3) * float("3.2"))  #resulat: il y a erreur , str et float ne peuvent pas interagir ensemeble
      TypeError: can't multiply sequence by non-int of type 'float'""")
    print()
    print(f' str(3/4) * 2 => {str(3/4) * 2}')
    