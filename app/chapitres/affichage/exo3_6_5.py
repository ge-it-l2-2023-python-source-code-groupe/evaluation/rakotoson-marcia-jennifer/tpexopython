def exo3_5():
    
    perc_GC = ((4500 + 2575)/14800)*100

    print(f"Le pourcentage de GC est {perc_GC:.0f} %")
    print(f"Le pourcentage de GC est {perc_GC:.1f} %")
    print(f"Le pourcentage de GC est {perc_GC:.2f} %")
    print(f"Le pourcentage de GC est {perc_GC:.3f} %")