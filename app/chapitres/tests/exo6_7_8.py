def exo7_8():
    liste=[[48.6, 53.4],[-124.9, 156.7],[-66.2, -30.8], \
    [-58.8, -43.1],[-73.9, -40.6],[-53.7, -37.5], \
    [-80.6, -26.0],[-68.5, 135.0],[-64.9, -23.5], \
    [-66.9, -45.5],[-69.6, -41.0],[-62.7, -37.5], \
    [-68.2, -38.3],[-61.2, -49.1],[-59.7, -41.1]]

    perfectphi=-57
    perfectpsi=-47

    for val in liste:
        if(perfectphi-30<=val[0]<=perfectphi+30 and perfectpsi-30<=val[1]<=perfectpsi+30):
            print(f"{val} est une hélice")
        else: print(f"{val}:n'est pas une hélice")