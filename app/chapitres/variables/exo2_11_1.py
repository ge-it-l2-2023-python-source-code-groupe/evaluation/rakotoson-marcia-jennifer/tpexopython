def Friedman():
    print(f"7 + 3**6= {7 +3**6}")
    print(f"C'est un nombre de Friedman")

    print(f"(3 + 4)**3= {(3 + 4)**3}")
    print(f"C'est un nombre de Friedman")

    print(f"3**6 - 5= {3**6 - 5}")
    print(f"C'est pas un nombre de Friedman")

    print(f"(1 + 2**8) * 5= {(1 + 2**8) * 5}")
    print(f"C'est un nombre de Friedman")

    print(f"(2 + 1**8)**7= {(2 + 1**8)**7}")
    print(f"C'est un nombre de Friedman")