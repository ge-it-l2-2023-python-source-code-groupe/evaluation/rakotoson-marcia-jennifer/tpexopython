def exo7_5():
    note=[14,9,13,15,12]

    som=sum(note)
    maximum=max(note)
    minimum=min(note)

    print(note)
    print(f" La somme de ses notes est :{som}")
    print(f"Le minimum est {minimum}")
    print(f" Le maximum est {maximum}")

    moy=som/len(note)

    if(moy>10 and moy>12): print(f" Moyenne de l'éleve: {moy:.2f} ,passable")
    elif(moy>12 and moy<14) : print(f"Moyenne de l'éleve: {moy:.2f}, assez bien")
    elif(moy>14): print(f"Moyenne de l'éleve: {moy:.2f} ,bien")

