def OperationResult():
    print(f"(1+2)**3 : resulat => 27")
    print(f"`Da` * 4: resultat => DaDaDaDa")
    print(f"`Da` + 3 : resultat => il y a erreur : casting de Int+String")
    print(f"(`Pa`+`La`) * 2 : resultat => PaLaPaLa ")
    print(f"(`Da`*4) / 2 : resulat: il y a erreur: l'opération n'est pas valide ")
    print(f"5/2 : resulat => 2.5")
    print(f"5 // 2 : resulat => 2")
    print(f"5%2 : resultat => 1")
    
    print()
    print("--------------------------------------")
    
    print("Resultat de l'interpreteur: ")
    
    print(f"(1+2)**3 => {(1+2)**3}")
    
    print("`Da` * 4 => ", "Da"*4)
    
    print("""
      ("Da" + 3):
      Traceback (most recent call last):
      File "/home/megumi/Documents/GEIT/L2/Python/Exo/variable(2)/exo2.11.2.py", line 3, in <module>
      print("Da" + 3) #resultat: il y a erreur : casting de Int+String
      TypeError: can only concatenate str (not "int") to str""")
    
    print()
    
    print("(`Pa`+`La`) * 2 => " ,("Pa"+"La") * 2)
          
    print(""" 
      ("Da"*4) / 2:
      Traceback (most recent call last):
      File "/home/megumi/Documents/GEIT/L2/Python/Exo/variable(2)/exo2.11.2.py", line 7, in <module>
      print(("Da"*4) / 2) #resulat: il y a erreur: l'opération n'est pas valide 
      TypeError: unsupported operand type(s) for /: 'str' and 'int' """)

    print()
    print(f"5/2 => {5/2}")
    
    print(f"5 // 2 => {5//2}")
    
    print(f"5%2 => {5%2}")
    